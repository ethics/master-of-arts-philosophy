Master of Arts
==============

Purpose
-------
The course is intended to train the mind for better logical thinking, mental 
discipline, ability for analysis and synthesis, critical reflection on social
and political realities. The course will also help in genuine appreciation 
of others, ‘worldviews’, better understanding of various thought patterns 
and peaceful co-existence.


Duration
--------
* Jan 2020 - Dec Dec 2022


Main Subjects
-------------

* Indian Philosophy
* Logic
* Ethics
* Epistemology
* Philosophy of human person
* Introduction to peace and conflict resolution
* Research methodology in philosophy
* Western philosophy
* Metaphysics
* Philosophy of mind
* Philosophy of technology
* Philosophy of arts
* Philosophy of science and cosmology
* Dissertation
